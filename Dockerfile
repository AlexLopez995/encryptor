# Use Python 3.10 Bullseye image as the base image
FROM python:3.10-bullseye

# Upgrade pip to the latest version
RUN python -m pip install --upgrade pip

# Set the working directory to /app inside the container
WORKDIR /app

# Copy the requirements.txt file into our working directory
COPY requirements.txt .

# Install dependencies from the requirements.txt file
RUN python -m pip install -r requirements.txt

# Copy the rest of the application's code into the container
COPY . .

# Expose the port the app runs on
EXPOSE 8000
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]
