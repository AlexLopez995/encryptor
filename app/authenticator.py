from datetime import datetime, timedelta
from jose import JWTError, jwt
from passlib.context import CryptContext
from pydantic import BaseModel
from pydantic_settings import BaseSettings
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from typing import Optional
from .database import SessionLocal


class UserToken(BaseModel):
    username: str


class Settings(BaseSettings):
    secret_key: str = "a_default_secret_key"
    algorithm: str = "HS256"
    access_token_expire_minutes: int = 30

    class Config:
        env_file = ".env"


settings = Settings()


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


def create_access_token(data: dict):
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(
        minutes=settings.access_token_expire_minutes
        )
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(
        to_encode,
        settings.secret_key,
        algorithm=settings.algorithm
        )
    return encoded_jwt


def decode_access_token(token: str) -> Optional[UserToken]:
    try:
        payload = jwt.decode(
            token,
            settings.secret_key,
            algorithms=[settings.algorithm])
        username: str = payload.get("sub")
        if username is None:
            return None
        return UserToken(username=username)
    except JWTError:
        return None


def get_current_user(token: str = Depends(oauth2_scheme)) -> UserToken:
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    user_token = decode_access_token(token)
    if user_token is None:
        raise credentials_exception
    return user_token


# def get_current_user(token: str = Depends(oauth2_scheme), db=Depends(get_db)):
#     credentials_exception = HTTPException(
#         status_code=status.HTTP_401_UNAUTHORIZED,
#         detail="Could not validate credentials",
#         headers={"WWW-Authenticate": "Bearer"},
#     )
#     try:
#         payload = jwt.decode(token, settings.secret_key, algorithms=[settings.algorithm])
#         username: str = payload.get("sub")
#         if username is None:
#             raise credentials_exception
#         user = db.query(models.User).filter(models.User.username == username).first()
#         if user is None:
#             raise credentials_exception
#         return user
#     except JWTError:
#         raise credentials_exception
