from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

SQLALCHEMY_DATABASE_URL = "postgresql://user:password@db/dbname"

engine = create_engine(SQLALCHEMY_DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()




# from sqlalchemy import create_engine
# from sqlalchemy.ext.declarative import declarative_base
# from sqlalchemy.orm import sessionmaker


# # Database URL configuration
# # Format for PostgreSQL: postgresql://user:password@localhost/dbname
# # Adjust the URL according to your database setup
# SQLALCHEMY_DATABASE_URL = "postgresql://user:password@localhost/dbname"

# # SQLAlchemy Engine
# engine = create_engine(
#     SQLALCHEMY_DATABASE_URL
# )


# # SessionLocal Class
# # This class will be used to create a database session for each request
# SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


# # Base Class
# # This class will serve as a base class for the ORM models
# Base = declarative_base()
