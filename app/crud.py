from sqlalchemy.orm import Session
from . import models
from . import authenticator
from app import schemas
from fastapi import HTTPException
from sqlalchemy.exc import IntegrityError, SQLAlchemyError
from starlette.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_409_CONFLICT
    )


def get_user_profile(db: Session, user_id: int):
    return db.query(models.User).filter(models.User.id == user_id).first()


def create_user(db: Session, username: str, password: str):
    try:
        existing_user = db.query(models.User).filter(
            models.User.username == username
            ).first()
        if existing_user:
            raise HTTPException(
                status_code=400, detail="Username already registered"
                )
        hashed_password = authenticator.get_password_hash(password)
        db_user = models.User(
            username=username,
            hashed_password=hashed_password
            )
        db.add(db_user)
        db.commit()
        db.refresh(db_user)
        return db_user
    except IntegrityError:
        db.rollback()
        raise HTTPException(
            status_code=HTTP_409_CONFLICT,
            detail="Username already taken, please choose another."
        )
    except SQLAlchemyError as e:
        db.rollback()
        print(f"Database error occurred: {e}")
        raise HTTPException(
            status_code=HTTP_400_BAD_REQUEST,
            detail="Could not process your request."
        )


def update_user_profile(
        db: Session, user_id: int,
        profile_data: schemas.UserProfileUpdate
        ):
    user = db.query(models.User).filter(models.User.id == user_id).first()
    if not user:
        raise HTTPException(
            status_code=404,
            detail="User not found"
        )
    if profile_data.email is not None:
        user.email = profile_data.email
    if profile_data.bio is not None:
        user.bio = profile_data.bio
    if profile_data.profile_picture is not None:
        user.profile_picture = profile_data.profile_picture
    db.add(user)
    db.commit()
    db.refresh(user)
    return user


def get_contacts(db: Session, user_id: int):
    return db.query(models.Contact).filter(
        models.Contact.user_id == user_id
        ).all()


def create_contact(db: Session, user_id: int, contact_user_id: int):
    if user_id == contact_user_id:
        raise HTTPException(
            status_code=400,
            detail="Cannot add yourself as contact"
            )
    existing_contact = db.query(models.Contact).filter(
        models.Contact.user_id == user_id,
        models.Contact.contact_user_id == contact_user_id
    ).first()
    if existing_contact:
        raise HTTPException(
            status_code=400,
            detail="Contact already exists"
        )
    db_contact = models.Contact(
        user_id=user_id, contact_user_id=contact_user_id
        )
    db.add(db_contact)
    db.commit()
    db.refresh(db_contact)
    return db_contact


def delete_contact(db: Session, user_id: int, contact_id: int):
    db_contact = db.query(models.Contact).filter(
            models.Contact.id == contact_id,
            models.Contact.user_id == user_id
        ).first()
    if not db_contact:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="Contact not found."
            )
    db.delete(db_contact)
    db.commit()
    return {"msg": "Contact deleted"}
    # try:
    #     db_contact = db.query(models.Contact).filter(
    #         models.Contact.id == contact_id,
    #         models.Contact.user_id == user_id
    #     ).first()
    #     if not db_contact:
    #         raise HTTPException(
    #             status_code=HTTP_404_NOT_FOUND,
    #             detail="Contact not found."
    #         )
    #     db.delete(db_contact)
    #     db.commit()
    #     return db_contact
    # except SQLAlchemyError as e:
    #     db.rollback()
    #     print(f"Database error occurred: {e}")
    #     raise HTTPException(
    #         status_code=HTTP_400_BAD_REQUEST,
    #         detail="Could not delete the contact."
    #     )


def get_conversations(db: Session, user_id: int):
    return db.query(models.Conversation).filter(
        models.Conversation.participants.any(id=user_id)
    ).all()


def create_conversation(db: Session, user_id: int, contact_user_id: int):
    db_conversation = models.Conversation(
        user_id=user_id, contact_user_id=contact_user_id
    )
    db.add(db_conversation)
    db.commit()
    db.refresh(db_conversation)
    return db_conversation


def delete_conversation(db: Session, conversation_id: int):
    db_conversation = db.query(models.Conversation).filter(
        models.Conversation.id == conversation_id
    ).first()
    db.delete(db_conversation)
    db.commit()
    return db_conversation


def get_messages(db: Session, conversation_id: int):
    return db.query(models.Message).filter(
        models.Message.conversation_id == conversation_id
        ).all()


def create_message(
        db: Session,
        conversation_id: int,
        sender_id: int,
        content: str
        ):
    db_message = models.Message(
        conversation_id=conversation_id,
        sender_id=sender_id,
        content=content
    )
    db.add(db_message)
    db.commit()
    db.refresh(db_message)
    return db_message
