from fastapi import FastAPI, Depends, HTTPException, status
from sqlalchemy.orm import Session
from app import schemas, models, authenticator, crud
from .database import SessionLocal, engine
from datetime import timedelta
from typing import List


models.Base.metadata.create_all(bind=engine)

app = FastAPI()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.post("/register/")
def register_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    new_user = models.User(
        username=user.username,
        hashed_password=authenticator.get_password_hash(user.password)
        )
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user


@app.post("/login/")
def login(user: schemas.UserLogin, db: Session = Depends(get_db)):
    db_user = db.query(models.User).filter(
        models.User.username == user.username).first()
    if db_user is None or not authenticator.verify_password(
        user.password,
        db_user.hashed_password
    ):
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Invalid username or password"
            )
    access_token_expires = timedelta(
        minutes=authenticator.ACCESS_TOKEN_EXPIRE_MINUTES
        )
    access_token = authenticator.create_access_token(
        data={"sub": user.username}
        )
    return {
        "access_token": access_token,
        "token_type": "bearer",
        "expires_in": access_token_expires.total_seconds()
    }


@app.post("/users/", response_model=schemas.User)
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    return crud.create_user(db, user.username, user.password)


@app.get("/users/{user_id}/profile", response_model=schemas.UserProfile)
def get_user_profile(user_id: int, db: Session = Depends(get_db)):
    user = crud.get_user_profile(db, user_id=user_id)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    return user


@app.put("/users/{user_id}/profile", response_model=schemas.UserProfile)
def update_user_profile(
    user_id: int,
    profile: schemas.UserProfileUpdate,
    db: Session = Depends(get_db)
     ):
    return crud.update_user_profile(db, user_id=user_id, profile_data=profile)


@app.get("/users/{user_id}", response_model=schemas.User)
def read_user(user_id: int, db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user


@app.get("/contacts/{user_id}", response_model=List[schemas.Contact])
def read_contacts(user_id: int, db: Session = Depends(get_db)):
    return crud.get_contacts(db, user_id=user_id)


@app.post("/contacts/", response_model=schemas.Contact)
def create_contact(contact: schemas.ContactCreate, db: Session = Depends(
    get_db
)):
    return crud.create_contact(
        db,
        user_id=contact.user_id,
        contact_user_id=contact.contact_user_id
        )


@app.get(
        "/users/{user_id}/contacts",
        response_model=List[schemas.ContactDisplay]
        )
def get_contacts(user_id: int, db: Session = Depends(get_db)):
    return crud.get_contacts(db, user_id=user_id)


@app.post("/users/{user_id}/contacts", response_model=schemas.ContactDisplay)
def add_contact(
    user_id: int,
    contact: schemas.ContactBase,
    db: Session = Depends(get_db)
):
    return crud.create_contact(
        db,
        user_id=user_id,
        contact_user_id=contact.contact_user_id
        )


@app.delete("/users/{users_id}/contacts/{contact_id}")
def remove_contact(user_id: int, contact_id: int, db: Session = Depends(get_db)
                   ):
    crud.delete_contact(db, user_id=user_id, contact_id=contact_id)
    return {"msg": "Contact deleted"}


@app.get(
        "/users/{user_id}/conversations",
        response_model=List[schemas.ConversationDisplay]
         )
def get_conversations(user_id: int, db: Session = Depends(get_db)):
    return crud.get_conversations(db, user_id=user_id)


@app.post(
        "/users/{user_id}/conversations",
        response_model=schemas.ConversationDisplay
        )
def create_conversation(
    user_id: int,
    conversation: schemas.ConversationBase,
    db: Session = Depends(get_db)
):
    return crud.create_conversation(
        db,
        user_id=user_id,
        title=conversation.title
    )


@app.get(
        "/conversations/{conversation_id}/messages",
        response_model=List[schemas.MessageDisplay]
        )
def get_messages(conversation_id: int, db: Session = Depends(get_db)):
    return crud.get_messages(db, conversation_id=conversation_id)


@app.post(
        "/conversations/{conversation_id}/messages",
        response_model=schemas.MessageDisplay
        )
def send_message(
    conversation_id: int,
    message: schemas.MessageCreate,
    db: Session = Depends(get_db)
):
    return crud.create_message(
        db,
        conversation_id=conversation_id,
        sender_id=message.sender_id,
        content=message.content
        )
