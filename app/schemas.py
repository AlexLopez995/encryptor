from pydantic import BaseModel
from typing import List, Optional
from datetime import datetime


class User(BaseModel):
    id: int
    username: str


class UserCreate(BaseModel):
    username: str
    password: str


class UserLogin(BaseModel):
    username: str
    password: str


class UserProfile(BaseModel):
    username: str
    email: Optional[str] = None
    bio: Optional[str] = None
    profile_picture: Optional[str] = None


class UserProfileUpdate(BaseModel):
    email: Optional[str] = None
    bio: Optional[str] = None
    profile_picture: Optional[str] = None


class Token(BaseModel):
    access_token: str
    token_type: str = "bearer"
    expires_in: int


class Profile(BaseModel):
    username: str
    profile_picture: str = None


class Contact(BaseModel):
    user_id: int
    contact_user_id: int


class ContactCreate(BaseModel):
    user_id: int
    contact_user_id: int


class ContactBase(BaseModel):
    contact_user_id: int


class ContactDisplay(ContactBase):
    id: int
    contact_user_id: int
    contact_username: str
    contact_email: Optional[str]

    class Config:
        from_attributes = True


class ConversationBase(BaseModel):
    title: Optional[str] = None


class ConversationDisplay(ConversationBase):
    id: int
    participants: List[Profile]
    created_at: datetime
    updated_at: datetime

    class Config:
        from_attributes = True


class MessageCreate(BaseModel):
    content: str


class MessageDisplay(MessageCreate):
    id: int
    content: str
    sender_id: int
    conversation_id: int
    sender: Profile
    created_at: datetime
    is_read: bool

    class Config:
        from_attributes = True
