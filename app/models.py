from sqlalchemy import Column, Integer, String, ForeignKey, Table
from sqlalchemy.orm import relationship
from .database import Base


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    email = Column(String, unique=True, index=True, nullable=True)
    bio = Column(String, nullable=True)
    profile_picture = Column(String, nullable=True)


class Contact(Base):
    __tablename__ = 'contacts'

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    contact_user_id = Column(Integer, ForeignKey('users.id'))
    user = relationship('User', back_populates='contacts')
    contact_user = relationship('User', back_populates='contacted_by')


class Conversation(Base):
    __tablename__ = 'conversations'

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    contact_user_id = Column(Integer, ForeignKey('users.id'))
    user = relationship('User', back_populates='conversations')
    contact_user = relationship('User', back_populates='conversations_with')


class Message(Base):
    __tablename__ = 'messages'

    id = Column(Integer, primary_key=True, index=True)
    sender_id = Column(Integer, ForeignKey('users.id'))
    receiver_id = Column(Integer, ForeignKey('users.id'))
    message = Column(String)
    sender = relationship('User', back_populates='sent_messages')
    receiver = relationship('User', back_populates='received_messages')
