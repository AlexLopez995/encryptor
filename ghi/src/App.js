import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Login from './components/Login';
import Register from './components/Register';
import Contacts from './components/Contacts';
import Chat from './components/Chat';

function App() {
    return (
        <Router>
            <Switch>
                <Route path="/login" component={Login} />
                <Route path="/register" component={Register} />
                <Route path="/contacts" component={Contacts} />
                <Route path="/chat/:conversationId" component={Chat} />
            </Switch>
        </Router>
    );
}

export default App;
