import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';


const Login = () => {
    const navigate = useNavigate();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();

        try {
            const response = await axios.post('http://localhost:8000/login', {
                username,
                password
            });
            const token = response.data.access_token;
            localStorage.setItem('token', token);
            navigate('/conversations');
        } catch (error) {
            setError("Login failed: " + error.response.data.detail);
        }
    };

    const createAccount = async () => {
        navigate("/register");
    }

    return (
        <div className="card mb-3 p-3">
			<div className="row no-gutters">
				<div className="card-body">

					<h1>Login</h1>
					{error ? <div className="alert alert-danger" role="alert">{error}</div> : null}

					<form onSubmit={(e) => handleSubmit(e)}>
						<div className="form-floating mb-3">
							<input

								autoComplete="true"
								id="username"
								name="username"
								placeholder="Username"
								type="text"
								className="form-control"
								onChange={(e) => setUsername(e.target.value)}
							/>
							<label htmlFor="username">Username</label>
						</div>
						<div className="form-floating mb-3">
							<input

								autoComplete="true"
								id="password"
								name="password"
								placeholder="Password"
								type="password"
								className="form-control"
								onChange={(e) => setPassword(e.target.value)}
							/>
							<label htmlFor="password">Password</label>
						</div>
						<div>
							<input className="btn" type="submit" value="Login" />
							<button className="btn mx-2" type="button" value="Create Account" onClick={createAccount} >Create Account</button>
						</div>
					</form>

				</div>
			</div>
		</div>
    );
}

export default Login;
